package com.birdboot.core;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 * 该线程任务负责与指定的客户端进行HTTP交互
 * HTTP协议要求浏览器与服务端采取"一问一答"的模式。对此，这里的处理流程分为三步:
 * 1:解析请求
 * 2:处理请求
 * 3:发送响应
 */
public class ClientHandler implements Runnable{
    private Socket socket;
    public ClientHandler(Socket socket){
        this.socket = socket;
    }

    public void run() {
        try {
            InputStream in = socket.getInputStream();
            int d;
            char pre='a',cur='a';//pre表示上次读取的字符，cur表示本次读取的字符
            StringBuilder builder = new StringBuilder();//保存读取后的所有字符
            while((d = in.read())!=-1){
                cur = (char)d;//本次读取的字符
                if(pre==13 && cur==10){//是否连续读取到了回车+换行
                    break;
                }
                builder.append(cur);//将本次读取的字符拼接
                pre=cur;//在进行下次读取前，将本次读取的字符保存到"上次读取的字符"中
            }
            String line = builder.toString().trim();
            System.out.println("请求行:"+line);

            //请求行相关信息
            String method;//请求方式
            String uri;//抽象路径
            String protocol;//协议版本
            //将请求行按照空格("\s"在正则表达式中表示一个空白字符，包含空格)拆分为三部分
            String[] data = line.split("\\s");
            method = data[0];
            uri = data[1];
            protocol = data[2];

            System.out.println("method:"+method);
            System.out.println("uri:"+uri);
            System.out.println("protocol:"+protocol);



        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}




