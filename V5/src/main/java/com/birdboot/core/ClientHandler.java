package com.birdboot.core;

import com.birdboot.http.HttpServletRequest;

import java.io.IOException;
import java.net.Socket;

/**
 * 该线程任务负责与指定的客户端进行HTTP交互
 * HTTP协议要求浏览器与服务端采取"一问一答"的模式。对此，这里的处理流程分为三步:
 * 1:解析请求
 * 2:处理请求
 * 3:发送响应
 */
public class ClientHandler implements Runnable {
    private Socket socket;

    public ClientHandler(Socket socket) {
        this.socket = socket;
    }

    public void run() {
        try {
            //1 解析请求
            //V4:将解析请求的细节移动到request构造器中进行
            HttpServletRequest request = new HttpServletRequest(socket);
            //获取请求的抽象路径
            String path = request.getUri();
            System.out.println(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}




