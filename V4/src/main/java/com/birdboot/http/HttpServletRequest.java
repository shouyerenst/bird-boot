package com.birdboot.http;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * V4:新增内容
 * 请求对象
 * 该类的每一个实例用于表示浏览器发送过来的一个HTTP请求
 * HTTP协议要求请求的格式由三部分构成:请求行，消息头，消息正文
 */
public class HttpServletRequest {
    private Socket socket;
    //请求行相关信息
    private String method;//请求方式
    private String uri;//抽象路径
    private String protocol;//协议版本

    //消息头相关信息 key:消息头名字  value:消息头对应的值
    private Map<String,String> headers = new HashMap<>();


    public HttpServletRequest(Socket socket) throws IOException {
        this.socket = socket;

        //1.1解析请求行
        String line = readLine();
        System.out.println("请求行:"+line);

        //将请求行按照空格("\s"在正则表达式中表示一个空白字符，包含空格)拆分为三部分
        String[] data = line.split("\\s");
        method = data[0];
        uri = data[1];
        protocol = data[2];

        System.out.println("method:"+method);
        System.out.println("uri:"+uri);
        System.out.println("protocol:"+protocol);

        //1.2解析消息头
        while(true) {
            line = readLine();
            if(line.isEmpty()){//如果读取到了空行
                break;
            }
            System.out.println("消息头:" + line);
            data = line.split(":\\s");
            headers.put(data[0],data[1]);
        }
        System.out.println("headers:"+headers);

    }




    /**
     * 通过socket获取的输入流读取客户端发送过来的一行字符串
     * @return
     */
    private String readLine() throws IOException {//通常被重用的代码不自己处理异常
        //对一个socket实例调用多次getInputStream()返回的始终是同一条输入流。而输出流也是如此
        InputStream in = socket.getInputStream();
        int d;
        char pre='a',cur='a';//pre表示上次读取的字符，cur表示本次读取的字符
        StringBuilder builder = new StringBuilder();//保存读取后的所有字符
        while((d = in.read())!=-1){
            cur = (char)d;//本次读取的字符
            if(pre==13 && cur==10){//是否连续读取到了回车+换行
                break;
            }
            builder.append(cur);//将本次读取的字符拼接
            pre=cur;//在进行下次读取前，将本次读取的字符保存到"上次读取的字符"中
        }
        return builder.toString().trim();
    }

    public String getMethod() {
        return method;
    }

    public String getUri() {
        return uri;
    }

    public String getProtocol() {
        return protocol;
    }

    /**
     * 根据给定的消息头的名字获取对应消息头的值
     * @param name
     * @return
     */
    public String getHeader(String name) {
        return headers.get(name);
    }
}





